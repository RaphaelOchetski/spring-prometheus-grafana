# Spring - Prometheus e Grafana



## Objetivo

A ideia por trás deste repositório é apenas colocar em prática as configurações básicas de monitoramento de uma aplicação Spring com Prometheus e Grafana.

## Execução

Para executar os serviços é necessário ter o Docker instalado (caso não tenha o Docker configurado na sua máquina, pode utilizar [este link](https://docs.docker.com/get-docker/) e seguir o passo a passo oficial).

Depois, é somente executar o seguinte comando:

```
docker-compose up
```
