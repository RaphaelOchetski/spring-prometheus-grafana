package com.raphael.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;


@SpringBootApplication
public class MonitoramentoApplication {

	final static Logger logger = LoggerFactory.getLogger(MonitoramentoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MonitoramentoApplication.class, args);
	}

	@GetMapping("/something")
	public ResponseEntity<String> createLogs() {
		logger.warn("Just checking");
		return ResponseEntity.ok().body("Ok");
	}

}
